#!/usr/bin/env ruby

dir = File.dirname(File.expand_path(__FILE__))
$LOAD_PATH.unshift dir + "/../lib"

require 'bundler/setup'
require "unimidi"

# Prompts the user to select a midi input
# Sends an inspection of the first 10 messages messages that input receives to standard out

num_messages = 10

# Prompt the user
input = UniMIDI::Input.gets

# using their selection...

puts "send some MIDI to your input now..."

while true do
  events = input.gets

  unless events.is_a?(Array) and events.empty?
    puts events.inspect
    events.each do |event|
      _nevim, key, value = event[:data]
      puts(key, value)

      case key
        when 7
          system %{osascript -e "set Volume #{value}"}
      end
    end
  end
end

puts "finished"
